import React, { 
    useState,
    useEffect }
    from 'react';
import { 
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Button,
    ScrollView }
    from 'react-native';

export default function home() {
  const [contador, setContador] = useState(0);

  const prato = () => {
    setContador(contador + 700);
  };

  const coca = () => {
    setContador(contador + 40);
  };

  const esteira = () => {
    setContador(contador - 250);
  };

     const [dado, setDado] = useState(0);
        const rodarDado = () => {
             setDado(Math.floor(Math.random() * 6) + 1);
        }

  return (
    <ScrollView>
    <View style={styles.container}>
      <View style={styles.flexContainer}>
        <Text style={styles.fonts}>Contador de calorias: {contador}</Text>
        <ScrollView>
        <Text style={styles.fonts}>Heitor</Text>
        <Text style={styles.fonts}>Vitão</Text>
        <Text style={styles.fonts}>Sep Vini</Text>
   
        </ScrollView>

        {/* <Image source={require('../meuPrimeiroApp/assets/vitao.png')} style={styles.imagem} /> */}
        <View style={styles.opcoesContainer}>
          <TouchableOpacity onPress={prato}>
            <Image source={require('../assets/prato.png')} style={styles.botaoImagem} />
          </TouchableOpacity>
          <TouchableOpacity onPress={coca}>
            <Image source={require('../assets/coca.png')} style={styles.botaoImagem} />
          </TouchableOpacity>
        </View>
        <Button onPress={esteira} title='30min na Esteira' />
      </View>

        <Text style={styles.fonts}>Número do dado: {dado}</Text>


        <Button
        onPress={rodarDado}
            title='Girar o dado'
        />

    </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#b2d1d6',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flexContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  opcoesContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  fonts: {
    fontSize: 24,
    color: 'blue',
    fontWeight: 'bold',
  },
  imagem: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
  },
  botaoImagem: {
    width: 150,
    height: 100,
    resizeMode: 'contain',
    marginRight: 10,
  },
});
